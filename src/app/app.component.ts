import { DataService } from './data.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Sort } from '@angular/material/sort';

interface Interest {
  id: number;
  isChecked: boolean;
  name: string;
}

interface User {
  user_id: number;
  first_name: string;
  last_name: string;
  age: number;
  registration_date: string;
  interests: Interest[];
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  displayedColumns: string[] = [
    'id',
    'first_name',
    'last_name',
    'age',
    'registration_date',
    'interests',
  ];
  dataSource: any;
  interests: Interest[] = [];
  users: User[] = [];

  @ViewChild(MatSort) sort: MatSort;

  ngOnInit() {
    this.getUsers();
    this.dataService.getInterests().subscribe((response) => {
      let interests = [];
      for (let i of response as []) {
        interests.push(i['interest']);
      }
      const interestsObj: Interest[] = [];
      for (let i of interests) {
        const interest: Interest = {
          id: this.interests.length + 1,
          isChecked: false,
          name: i,
        };
        interestsObj.push(interest);
      }
      this.interests.push(...interestsObj);
    });
  }

  constructor(private dataService: DataService) {}

  getUsers(active?: string, direction?: string) {
    const interests = this.interests.filter((i) => i.isChecked == true);
    this.dataService
      .getUsers(interests, active, direction)
      .subscribe((response) => {
        this.users.push(response as User);
        this.dataSource = new MatTableDataSource(response as []);
        this.dataSource.sortData = this.sortData;
      });
  }

  onCheckboxChange() {
    this.getUsers();
  }

  sortData(sort: Sort) {
    this.getUsers(sort.active, sort.direction);
  }
}

import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';

// const url = 'http://localhost:5000';
const url = 'https://interests-filter.herokuapp.com';

@Injectable({
  providedIn: 'root',
})
export class DataService {
  constructor(private http: HttpClient) {}

  getUsers(interests: any[], active?: string, direction?: string) {
    let params = new HttpParams();

    for (let i of interests) {
      params = params.append('interests', i.name);
    }

    if (active) {
      params = params.append('sort_by', active);
    }
    if (direction) {
      params = params.append('order_by', direction);
    }

    return this.http.get(`${url}/users`, { params });
  }

  getInterests() {
    return this.http.get(`${url}/interests`);
  }
}
